package com.dream21th.dream21thredis.result;

import java.util.Objects;

public class Result<T> {
	private String code;
	private String msg;
	private T data;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public T getData() {
		return data;
	}
	public void setData(T data) {
		this.data = data;
	}
	public Result(String code, String msg, T data) {
		super();
		this.code = code;
		this.msg = msg;
		this.data = data;
	}
	public Result() {
		super();
	}
	
	private Result(T data) {
		this.code="0";
		this.msg="success";
		this.data=data;
	}
	
	private Result(CodeMsg cm) {
		if(Objects.isNull(cm)){
			return;
		}
		this.code=cm.getCode();
		this.msg=cm.getMsg();
	}
	public static <T> Result<T> success(T data){
		return new Result<T>(data);
	}
	
	public static <T> Result<T> error(CodeMsg cm){
		return new Result<T>(cm);
	}
	
}
