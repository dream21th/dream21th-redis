package com.dream21th.dream21thredis.result;

public class CodeMsg {
	private String code;
	private String msg;

	public static CodeMsg SUCCESS=new CodeMsg("0", "success");
	public static CodeMsg SERVER_ERROR=new CodeMsg("500100", "服务内部错误");
	
	public CodeMsg() {
	}

	private CodeMsg(String code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	
}
