package com.dream21th.dream21thredis.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @Package:  com.jfpal.finance.risk.common.config.restTemplate
 * @ClassName:      
 * @Description:
 * @Author:         jian.liu
 * @CreateDate:     2018/4/24 23:14
 * @UpdateUser:     jian.liu
 * @UpdateDate:     2018/4/24 23:14
 * @UpdateRemark:   The modified content
 * @Version:        1.0
 */
@SpringBootConfiguration
public class RestTemplateConfig {
  
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
 
}
