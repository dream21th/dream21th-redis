package com.dream21th.dream21thredis.config;

import lombok.Data;

@Data
public class Cluster {
	private String nodes;
	private boolean testOnReturn;
	private int maxIdle;
	private int minIdle;
	private int maxWaitMills;
	private boolean testOnBorrow;
	private int maxTotal;
	private int connectionTimeOut;
	
	public boolean getTestOnBorrow(){
		return testOnBorrow;
	}
	
	public void setTestOnBorrow(boolean testOnBorrow){
		this.testOnBorrow=testOnBorrow;
	}
}
