package com.dream21th.dream21thredis.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.dream21th.dream21thredis.handler.EncodeHandler;
import com.dream21th.dream21thredis.handler.ParamsHandler;
import com.dream21th.dream21thredis.handler.filter.HttpFilter;


@Configuration
public class WebSecurityConfig extends WebMvcConfigurerAdapter {

    /**
     * 登录session key
     */
    public static final String SESSION_KEY = "user";

    @Bean
    public EncodeHandler getEncodeHandler() {
        return new EncodeHandler();
    }
    
    @Bean
    public ParamsHandler getParamsHandler() {
        return new ParamsHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        InterceptorRegistration addInterceptor = registry.addInterceptor(getEncodeHandler());
        registry.addInterceptor(getParamsHandler());  
        // 排除配置
        addInterceptor.excludePathPatterns("/error");
        addInterceptor.excludePathPatterns("/login**");

        // 拦截配置
        addInterceptor.addPathPatterns("/**");
    }
    
    @Bean
	public FilterRegistrationBean httpFilter(){
		FilterRegistrationBean filterRegistrationBean=new FilterRegistrationBean();
		filterRegistrationBean.setFilter(new HttpFilter());
		filterRegistrationBean.addUrlPatterns("*");
		return filterRegistrationBean;
	}
}
