package com.dream21th.dream21thredis.dto;

import java.util.List;
import java.util.Map;

import lombok.Data;

@Data
public class MutiDto {

	private String code;
	
	private Student student;
	
	private List<Person> persons;
	
	private Map<String,String> map;
}
