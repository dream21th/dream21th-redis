package com.dream21th.dream21thredis.dto;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.io.Serializable; 

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Student implements BaseInfo,Serializable{
	private static final long serialVersionUID = 1L;
	private String name;
	private String phone;
	private String sex;
	private Integer age;
	private Date birthday;
}
