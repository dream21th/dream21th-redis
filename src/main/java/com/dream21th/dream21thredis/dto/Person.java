package com.dream21th.dream21thredis.dto;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Person implements BaseInfo,Serializable {
	private static final long serialVersionUID = 4363768366159512499L;
	private String name;
	private String phone;
	private String sex;
	private String address;
	private String dream;
	private String isMarry;
}
