package com.dream21th.dream21thredis.redis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dream21th.dream21thredis.key.KeyPrefix;
import com.dream21th.dream21thredis.util.ObjectUtils;
import com.dream21th.dream21thredis.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;

/**
 * 集群方式redis客户端操作
 * @author dream21th
 *
 */
@Slf4j
@Service("jedisClusterClient")
public class JedisClusterClient implements IJedisClient {
	@Autowired
	private  JedisCluster jedisCluster ;

	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#get(java.lang.String)
	 */
	
	/**
	 * 获取对象值
	 * */
	public <T> T get(KeyPrefix prefix, String key,  Class<T> clazz) {
		 try {
			 //key
			 String realKey  = prefix.getPrefix() + key;
			 String  str = jedisCluster.get(realKey);
			 T t = ObjectUtils.stringToBean(str, clazz);
			 return t;
		 }finally {
			 //returnResource(jedis);
		 }
	}
	
	/**
	 * 给对象设定值
	 * */
	public <T> boolean set(KeyPrefix prefix, String key,  T value) {
		 try {
			 String str =ObjectUtils.beanToString(value);
			 if(str == null || str.length() <= 0) {
				 return false;
			 }
			//key
			 String realKey  = prefix.getPrefix() + key;
			 int seconds =  prefix.expireSeconds();
			 if(seconds <= 0) {
				 jedisCluster.set(realKey, str);
			 }else {
				 jedisCluster.setex(realKey, seconds, str);
			 }
			 return true;
		 }finally {
			 //returnResource(jedis);
		 }
	}
	
	/**
	 * 判断键值是否存在
	 * */
	public <T> boolean exists(KeyPrefix prefix, String key) {
		 try {
			//key
			 String realKey  = prefix.getPrefix() + key;
			return  jedisCluster.exists(realKey);
		 }finally {
			 //returnResource(jedis);
		 }
	}
	
	/**
	 * 增加一个值
	 * */
	public <T> Long incr(KeyPrefix prefix, String key) {
		 try {
			String realKey  = prefix.getPrefix() + key;
			return  jedisCluster.incr(realKey);
		 }finally {
			 //returnResource(jedis);
		 }
	}
	
	/**
	 * 减少一个值
	 * */
	public <T> Long decr(KeyPrefix prefix, String key) {
		 try {
			//key
			 String realKey  = prefix.getPrefix() + key;
			return  jedisCluster.decr(realKey);
		 }finally {
			// returnResource(jedis);
		 }
	}
	
	@Override
	public  String get(String key) {
		String value = null;
		try {
			if (jedisCluster.exists(key)) {
				value = jedisCluster.get(key);
				value = StringUtils.isNotBlank(value) && !"nil".equalsIgnoreCase(value) ? value : null;
				log.debug("get {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("get {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getObject(java.lang.String)
	 */
	@Override
	public   Object getObject(String key) {
		Object value = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				value = toObject(jedisCluster.get(getBytesKey(key)));
				log.debug("getObject {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObject {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
//			try {
//				jedisCluster.close();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#set(java.lang.String, java.lang.String, int)
	 */
	@Override
	public   String set(String key, String value, int cacheSeconds) {
		String result = null;
	
		try {
		
			result = jedisCluster.set(key, value);
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("set {} = {}", key, value);
		} catch (Exception e) {
			log.warn("set {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setObject(java.lang.String, java.lang.Object, int)
	 */
	@Override
	public   String setObject(String key, Object value, int cacheSeconds) {
		String result = null;
		
		try {
			result = jedisCluster.set(getBytesKey(key), toBytes(value));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setObject {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObject {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getList(java.lang.String)
	 */
	@Override
	public   List<String> getList(String key) {
		List<String> value = null;
		try {
			if (jedisCluster.exists(key)) {
				value = jedisCluster.lrange(key, 0, -1);
				log.debug("getList {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getList {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getObjectList(java.lang.String)
	 */
	@Override
	public   List<Object> getObjectList(String key) {
		List<Object> value = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				List<byte[]> list = jedisCluster.lrange(getBytesKey(key), 0, -1);
				value = new ArrayList<Object>();
				for (byte[] bs : list){
					value.add(toObject(bs));
				}
				log.debug("getObjectList {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectList {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setList(java.lang.String, java.util.List, int)
	 */
	@Override
	public   long setList(String key, List<String> value, int cacheSeconds) {
		long result = 0;
		try {
			if (jedisCluster.exists(key)) {
				jedisCluster.del(key);
			}
			result = jedisCluster.rpush(key, value.toArray(new String[0]));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setList {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setList {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setObjectList(java.lang.String, java.util.List, int)
	 */
	@Override
	public   long setObjectList(String key, List<Object> value, int cacheSeconds) {
		long result = 0;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				jedisCluster.del(key);
			}
			List<byte[]> list = new ArrayList<byte[]>();
			for (Object o : value){
				list.add(toBytes(o));
			}
			result = jedisCluster.rpush(getBytesKey(key), list.toArray(new byte[0][]));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setObjectList {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectList {} = {}", key, value, e);
		} finally {
			//returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#listAdd(java.lang.String, java.lang.String)
	 */
	@Override
	public   long listAdd(String key, String... value) {
		long result = 0;
		try {
			result = jedisCluster.rpush(key, value);
			log.debug("listAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("listAdd {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#listObjectAdd(java.lang.String, java.lang.Object)
	 */
	@Override
	public   long listObjectAdd(String key, Object... value) {
		long result = 0;
		try {
			List<byte[]> list =new ArrayList<byte[]>();
			for (Object o : value){
				list.add(toBytes(o));
			}
			result = jedisCluster.rpush(getBytesKey(key), list.toArray(new byte[0][]));
			log.debug("listObjectAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("listObjectAdd {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getSet(java.lang.String)
	 */
	@Override
	public   Set<String> getSet(String key) {
		Set<String> value = null;
		try {
			if (jedisCluster.exists(key)) {
				value = jedisCluster.smembers(key);
				log.debug("getSet {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getSet {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getObjectSet(java.lang.String)
	 */
	@Override
	public   Set<Object> getObjectSet(String key) {
		Set<Object> value = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				value = new HashSet<Object>();
				Set<byte[]> set = jedisCluster.smembers(getBytesKey(key));
				for (byte[] bs : set){
					value.add(toObject(bs));
				}
				log.debug("getObjectSet {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectSet {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setSet(java.lang.String, java.util.Set, int)
	 */
	@Override
	public   long setSet(String key, Set<String> value, int cacheSeconds) {
		long result = 0;
		try {
			if (jedisCluster.exists(key)) {
				jedisCluster.del(key);
			}
			result = jedisCluster.sadd(key, value.toArray(new String[0]));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setSet {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setSet {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setObjectSet(java.lang.String, java.util.Set, int)
	 */
	@Override
	public   long setObjectSet(String key, Set<Object> value, int cacheSeconds) {
		long result = 0;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				jedisCluster.del(key);
			}
			Set<byte[]> set = new HashSet<byte[]>();
			for (Object o : value){
				set.add(toBytes(o));
			}
			result = jedisCluster.sadd(getBytesKey(key), set.toArray(new byte[0][]));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setObjectSet {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectSet {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setSetAdd(java.lang.String, java.lang.String)
	 */
	@Override
	public   long setSetAdd(String key, String... value) {
		long result = 0;
		try {
			result = jedisCluster.sadd(key, value);
			log.debug("setSetAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setSetAdd {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setSetObjectAdd(java.lang.String, java.lang.Object)
	 */
	@Override
	public   long setSetObjectAdd(String key, Object... value) {
		long result = 0;
		try {
			Set<byte[]> set = new HashSet<byte[]>();
			for (Object o : value){
				set.add(toBytes(o));
			}
			result = jedisCluster.rpush(getBytesKey(key), set.toArray(new byte[0][]));
			log.debug("setSetObjectAdd {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setSetObjectAdd {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getMap(java.lang.String)
	 */
	@Override
	public   Map<String, String> getMap(String key) {
		Map<String, String> value = null;
		try {
			if (jedisCluster.exists(key)) {
				value = jedisCluster.hgetAll(key);
				log.debug("getMap {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getMap {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#getObjectMap(java.lang.String)
	 */
	@Override
	public   Map<String, Object> getObjectMap(String key) {
		Map<String, Object> value = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				value =new HashMap<String, Object>();
				Map<byte[], byte[]> map = jedisCluster.hgetAll(getBytesKey(key));
				for (Map.Entry<byte[], byte[]> e : map.entrySet()){
					value.put(StringUtils.toString(e.getKey()), toObject(e.getValue()));
				}
				log.debug("getObjectMap {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObjectMap {} = {}", key, value, e);
		} finally {
		}
		return value;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setMap(java.lang.String, java.util.Map, int)
	 */
	@Override
	public   String setMap(String key, Map<String, String> value, int cacheSeconds) {
		String result = null;
		try {
			if (jedisCluster.exists(key)) {
				jedisCluster.del(key);
			}
			result = jedisCluster.hmset(key, value);
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setMap {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setMap {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#setObjectMap(java.lang.String, java.util.Map, int)
	 */
	@Override
	public   String setObjectMap(String key, Map<String, Object> value, int cacheSeconds) {
		String result = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				jedisCluster.del(key);
			}
			Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
			for (Map.Entry<String, Object> e : value.entrySet()){
				map.put(getBytesKey(e.getKey()), toBytes(e.getValue()));
			}
			result = jedisCluster.hmset(getBytesKey(key), (Map<byte[], byte[]>)map);
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setObjectMap {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObjectMap {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapPut(java.lang.String, java.util.Map)
	 */
	@Override
	public   String mapPut(String key, Map<String, String> value) {
		String result = null;
		try {
			result = jedisCluster.hmset(key, value);
			log.debug("mapPut {} = {}", key, value);
		} catch (Exception e) {
			log.warn("mapPut {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapObjectPut(java.lang.String, java.util.Map)
	 */
	@Override
	public   String mapObjectPut(String key, Map<String, Object> value) {
		String result = null;
		try {
			Map<byte[], byte[]> map = new HashMap<byte[], byte[]>();
			for (Map.Entry<String, Object> e : value.entrySet()){
				map.put(getBytesKey(e.getKey()), toBytes(e.getValue()));
			}
			result = jedisCluster.hmset(getBytesKey(key), (Map<byte[], byte[]>)map);
			log.debug("mapObjectPut {} = {}", key, value);
		} catch (Exception e) {
			log.warn("mapObjectPut {} = {}", key, value, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapRemove(java.lang.String, java.lang.String)
	 */
	@Override
	public   long mapRemove(String key, String mapKey) {
		long result = 0;
		try {
			result = jedisCluster.hdel(key, mapKey);
			log.debug("mapRemove {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapRemove {}  {}", key, mapKey, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapObjectRemove(java.lang.String, java.lang.String)
	 */
	@Override
	public   long mapObjectRemove(String key, String mapKey) {
		long result = 0;
		try {
			result = jedisCluster.hdel(getBytesKey(key), getBytesKey(mapKey));
			log.debug("mapObjectRemove {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapObjectRemove {}  {}", key, mapKey, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapExists(java.lang.String, java.lang.String)
	 */
	@Override
	public   boolean mapExists(String key, String mapKey) {
		boolean result = false;
		try {
			result = jedisCluster.hexists(key, mapKey);
			log.debug("mapExists {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapExists {}  {}", key, mapKey, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#mapObjectExists(java.lang.String, java.lang.String)
	 */
	@Override
	public   boolean mapObjectExists(String key, String mapKey) {
		boolean result = false;
		try {
			result = jedisCluster.hexists(getBytesKey(key), getBytesKey(mapKey));
			log.debug("mapObjectExists {}  {}", key, mapKey);
		} catch (Exception e) {
			log.warn("mapObjectExists {}  {}", key, mapKey, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#del(java.lang.String)
	 */
	@Override
	public   long del(String key) {
		long result = 0;
		try {
			if (jedisCluster.exists(key)){
				result = jedisCluster.del(key);
				log.debug("del {}", key);
			}else{
				log.debug("del {} not exists", key);
			}
		} catch (Exception e) {
			log.warn("del {}", key, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#delObject(java.lang.String)
	 */
	@Override
	public   long delObject(String key) {
		long result = 0;
		try {
			if (jedisCluster.exists(getBytesKey(key))){
				result = jedisCluster.del(getBytesKey(key));
				log.debug("delObject {}", key);
			}else{
				log.debug("delObject {} not exists", key);
			}
		} catch (Exception e) {
			log.warn("delObject {}", key, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#exists(java.lang.String)
	 */
	@Override
	public   boolean exists(String key) {
		boolean result = false;
		try {
			result = jedisCluster.exists(key);
			log.debug("exists {}", key);
		} catch (Exception e) {
			log.warn("exists {}", key, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}
	
	/* (non-Javadoc)
	 * @see com.jeeplus.common.redis.IJedisClient#existsObject(java.lang.String)
	 */
	@Override
	public   boolean existsObject(String key) {
		boolean result = false;
		try {
			result = jedisCluster.exists(getBytesKey(key));
			log.debug("existsObject {}", key);
		} catch (Exception e) {
			log.warn("existsObject {}", key, e);
		} finally {
//			returnResource(jedis);
		}
		return result;
	}

	
	/**
	 * 获取byte[]类型Key
	 * @param key
	 * @return
	 */
	public static  byte[] getBytesKey(Object object){
		if(object instanceof String){
    		return StringUtils.getBytes((String)object);
    	}else{
    		return ObjectUtils.serialize(object);
    	}
	}
	
	/**
	 * Object转换byte[]类型
	 * @param key
	 * @return
	 */
	public static  byte[] toBytes(Object object){
    	return ObjectUtils.serialize(object);
	}

	/**
	 * byte[]型转换Object
	 * @param key
	 * @return
	 */
	public static Object toObject(byte[] bytes){
		return ObjectUtils.unserialize(bytes);
	}

	@Override
	public <T> T getT(String key) {
		T value = null;
		try {
			if (jedisCluster.exists(getBytesKey(key))) {
				value = toT(jedisCluster.get(getBytesKey(key)));
				log.debug("getObject {} = {}", key, value);
			}
		} catch (Exception e) {
			log.warn("getObject {} = {}", key, value, e);
		} finally {
			
		}
		return value;
	}

	@Override
	public <T> String setT(String key, T value, int cacheSeconds) {
		String result = null;
		try {
			result = jedisCluster.set(getBytesKey(key), toBytes(value));
			if (cacheSeconds != 0) {
				jedisCluster.expire(key, cacheSeconds);
			}
			log.debug("setObject {} = {}", key, value);
		} catch (Exception e) {
			log.warn("setObject {} = {}", key, value, e);
		} finally {
		}
		return result;
	}
	
	/**
	 * byte[]型转换T
	 * 
	 * @param key
	 * @return
	 */
	public static <T> T toT(byte[] bytes) {
		return ObjectUtils.unserializeT(bytes);
	}

}