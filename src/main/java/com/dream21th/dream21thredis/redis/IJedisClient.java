package com.dream21th.dream21thredis.redis;

import java.util.List;
import java.util.Map;
import java.util.Set;
import com.dream21th.dream21thredis.key.KeyPrefix;


/**
 * 
 * @author dream21th   采用jedis操作redis数据库提供的方法
 *
 */
public interface IJedisClient {
	/**
	 * 获取缓存   
	 * @param prefix   键的前缀
	 * @param key      键的值
	 * @param clazz    获取对象类型
	 * @return
	 */
	public <T> T get(KeyPrefix prefix, String key,  Class<T> clazz);
	
	/**
	 * 设置缓存
	 * @param prefix   键的前缀
	 * @param key      键的值
	 * @param value    设置的值
	 * @return
	 */
	public <T> boolean set(KeyPrefix prefix, String key,  T value);
	
	/**
	 * 判断缓存是否存在
	 * @param prefix
	 * @param key
	 * @return
	 */
	public <T> boolean exists(KeyPrefix prefix, String key);
	
	/**
	 * 键的值加1
	 * @param prefix
	 * @param key
	 * @return
	 */
	public <T> Long incr(KeyPrefix prefix, String key);
	
	/**
	 * 键的值减1
	 * @param prefix
	 * @param key
	 * @return
	 */
	public <T> Long decr(KeyPrefix prefix, String key);
	/**
	 * 获取缓存
	 * @param key 键
	 * @return 值
	 */

	public String get(String key);

	/**
	 * 获取缓存
	 * @param key 键
	 * @return 值
	 */
	public Object getObject(String key);
	
	/**
	 * 获取缓存
	 * @param key 键
	 * @return 值
	 */
	public <T> T getT(String key);

	/**
	 * 设置缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public String set(String key, String value, int cacheSeconds);

	/**
	 * 设置缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public String setObject(String key, Object value, int cacheSeconds);
	
	/**
	 * 设置缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public <T> String setT(String key, T value, int cacheSeconds);

	/**
	 * 获取List缓存
	 * @param key 键
	 * @return 值
	 */
	public List<String> getList(String key);

	/**
	 * 获取List缓存
	 * @param key 键
	 * @return 值
	 */
	public List<Object> getObjectList(String key);

	/**
	 * 设置List缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public long setList(String key, List<String> value, int cacheSeconds);

	/**
	 * 设置List缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public long setObjectList(String key, List<Object> value, int cacheSeconds);

	/**
	 * 向List缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long listAdd(String key, String... value);

	/**
	 * 向List缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long listObjectAdd(String key, Object... value);

	/**
	 * 获取缓存
	 * @param key 键
	 * @return 值
	 */
	public Set<String> getSet(String key);

	/**
	 * 获取缓存
	 * @param key 键
	 * @return 值
	 */
	public Set<Object> getObjectSet(String key);

	/**
	 * 设置Set缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public long setSet(String key, Set<String> value, int cacheSeconds);

	/**
	 * 设置Set缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public long setObjectSet(String key, Set<Object> value, int cacheSeconds);

	/**
	 * 向Set缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long setSetAdd(String key, String... value);

	/**
	 * 向Set缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long setSetObjectAdd(String key, Object... value);

	/**
	 * 获取Map缓存
	 * @param key 键
	 * @return 值
	 */
	public Map<String, String> getMap(String key);

	/**
	 * 获取Map缓存
	 * @param key 键
	 * @return 值
	 */
	public Map<String, Object> getObjectMap(String key);

	/**
	 * 设置Map缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public String setMap(String key, Map<String, String> value, int cacheSeconds);

	/**
	 * 设置Map缓存
	 * @param key 键
	 * @param value 值
	 * @param cacheSeconds 超时时间，0为不超时
	 * @return
	 */
	public String setObjectMap(String key, Map<String, Object> value,
			int cacheSeconds);

	/**
	 * 向Map缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public String mapPut(String key, Map<String, String> value);

	/**
	 * 向Map缓存中添加值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public String mapObjectPut(String key, Map<String, Object> value);

	/**
	 * 移除Map缓存中的值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long mapRemove(String key, String mapKey);

	/**
	 * 移除Map缓存中的值
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public long mapObjectRemove(String key, String mapKey);

	/**
	 * 判断Map缓存中的Key是否存在
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public boolean mapExists(String key, String mapKey);

	/**
	 * 判断Map缓存中的Key是否存在
	 * @param key 键
	 * @param value 值
	 * @return
	 */
	public boolean mapObjectExists(String key, String mapKey);

	/**
	 * 删除缓存
	 * @param key 键
	 * @return
	 */
	public long del(String key);

	/**
	 * 删除缓存
	 * @param key 键
	 * @return
	 */
	public long delObject(String key);

	/**
	 * 缓存是否存在
	 * @param key 键
	 * @return
	 */
	public boolean exists(String key);

	/**
	 * 缓存是否存在
	 * @param key 键
	 * @return
	 */
	public boolean existsObject(String key);

}
