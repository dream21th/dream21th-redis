package com.dream21th.dream21thredis.key;

public interface KeyPrefix {
		
	public int expireSeconds();
	
	public String getPrefix();
	
}
