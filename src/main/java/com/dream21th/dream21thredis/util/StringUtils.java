package com.dream21th.dream21thredis.util;

public class StringUtils {
	
	public static boolean isNotBlank(Object obj){
		if(obj==null)
			return false;
		return true;
	}
	
	public static String toString(Object obj){
		if(obj==null){
			return null;
		}
		return obj.toString();
	}

	public static byte[] getBytes(String obj){
		if(obj==null){
			return null;
		}
		return obj.getBytes();
	}
}
