package com.dream21th.dream21thredis.aspect;

import java.lang.reflect.Method;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.dream21th.dream21thredis.annotation.Encode;
import com.dream21th.dream21thredis.annotation.RedisDisLock;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Aspect
@Order(1)
@Component
public class EncodeAspect {

	@Pointcut("@annotation(com.dream21th.dream21thredis.annotation.Encode)")
	public void pointCut(){
		
	};
	
	@Around("pointCut()")
	public Object logStart(ProceedingJoinPoint joinPoint){
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Encode annotation = method.getAnnotation(Encode.class);
		Object[] args = joinPoint.getArgs();
		String[] paramNames=((CodeSignature)joinPoint.getSignature()).getParameterNames();
		Class[] classs=((CodeSignature)joinPoint.getSignature()).getParameterTypes();
		
		
		try {
			Object result=joinPoint.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
	}
}
