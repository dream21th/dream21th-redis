package com.dream21th.dream21thredis.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.dream21th.dream21thredis.dto.Student;
import com.dream21th.dream21thredis.key.UserKey;
import com.dream21th.dream21thredis.redis.IJedisClient;

@Component
public class RedisService {

	@Autowired
	private IJedisClient jedisClient;
	
	public void getResult(){
		Student student1=Student.builder().age(10).birthday(new Date()).sex("男").phone("1399999999").build();
		Student student2=Student.builder().age(20).birthday(new Date()).sex("女").phone("1398888888").build();
		List<Student> list=new ArrayList<>();
		list.add(student1);
		list.add(student2);
		jedisClient.set(UserKey.getById,"testList", list);
	}
	
	public List<Student> getList(){
		return jedisClient.get(UserKey.getById,"testList", List.class);
	}
	
}
