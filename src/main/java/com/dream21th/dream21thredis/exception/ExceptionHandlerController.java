package com.dream21th.dream21thredis.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import com.dream21th.dream21thredis.result.Result;
import javax.servlet.http.HttpServletRequest;

@Slf4j
@ControllerAdvice
@RestController
@SuppressWarnings("all")
public class ExceptionHandlerController {

	@ExceptionHandler({ FlowException.class })
	private Result<String> processFinanceUserException(HttpServletRequest request, FlowException e) {
		log.info("---------------------FlowException-");
		return new Result<String>(e.getCode(), e.getMsg(), e.getMsg());
	}


}
