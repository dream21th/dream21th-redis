package com.dream21th.dream21thredis.exception;


public class FlowException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	private String msg;
    private String code = "500";
    
    public FlowException(String msg) {
		super(msg);
		this.msg = msg;
	}
	
	public FlowException(String msg, Throwable e) {
		super(msg, e);
		this.msg = msg;
	}
	
	public FlowException(String msg, String code) {
		super(msg);
		this.msg = msg;
		this.code = code;
	}
	
	public FlowException(String msg, String code, Throwable e) {
		super(msg, e);
		this.msg = msg;
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
