package com.dream21th.dream21thredis.controller;

import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dream21th.dream21thredis.dto.Person;
import com.dream21th.dream21thredis.dto.Student;
import com.dream21th.dream21thredis.key.UserKey;
import com.dream21th.dream21thredis.redis.IJedisClient;
import com.dream21th.dream21thredis.result.Result;
import com.dream21th.dream21thredis.service.RedisService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/singleNode")
@Api(tags="单节点redis操作")
public class RedisSingleNodeController {

	@Autowired
	private IJedisClient jedisClient;
	@Autowired
	private RedisService redisService;
	
    @PostMapping("/mapTest/{key}")
    @ApiOperation(value = "map测试", notes = "map测试")
	public Result<Map<String,String>> mapTest(@PathVariable("key") String key,@RequestBody Map<String,String> map){
		jedisClient.setMap(key, map,0);
		return Result.success(jedisClient.getMap(key));
	}
	
    @PostMapping("/listTest/{key}")
	public Result<List<String>> ListTest(@PathVariable("key") String key,@RequestBody List<String> list){
		jedisClient.setList(key, list, 0);
		return Result.success(jedisClient.getList(key));
	}
    
    @PostMapping("/listTestObj/{key}")
	public Result<List<Object>> listTestObj(@PathVariable("key") String key,@RequestBody List<Object> list){
		jedisClient.setObjectList(key, list, 0);
		return Result.success(jedisClient.getObjectList(key));
	}
    
    @PostMapping("/listTestT")
	public  Result<Student> listTestT(@RequestBody Student t){
		jedisClient.set(UserKey.getById, "name",t);
		log.info("键值是否存在：{}",jedisClient.exists(UserKey.getById, "name"));
		redisService.getResult();
		return Result.success(jedisClient.get(UserKey.getById, "name",Student.class));
	}
    
    @PostMapping("/decIcrTest")
	public  Result<Long> decIcrTest(long num){
		jedisClient.set(UserKey.getById, "count",num);
		log.info("键值是否存在：{}",jedisClient.exists(UserKey.getById, "count"));
		log.info("加1后值：{}",jedisClient.incr(UserKey.getById, "count"));
		log.info("减1后值：{}",jedisClient.decr(UserKey.getById, "count"));
		return Result.success(jedisClient.get(UserKey.getById, "count",Long.class));
	}
    
    @PostMapping("/testObj")
	public  Result<Student> testObj(@RequestBody Student t){
    	jedisClient.setObject("student", t, 0);
		return Result.success((Student)jedisClient.getObject("student"));
	}
    
    @PostMapping("/testT")
	public  Result<Student> testT(@RequestBody Student t){
    	jedisClient.setObject("student", t, 0);
		return Result.success(jedisClient.getT("student"));
	}
    
    @PostMapping("/testT1")
	public  Result<Person> testT1(@RequestBody Person t){
    	jedisClient.setT("person", t, 0);
		return Result.success(jedisClient.getT("person"));
	}
    
    @PostMapping("/testList")
	public  Result<List<Student>> testList(@RequestBody Student t){
		return Result.success(redisService.getList());
	}
    
    @PostMapping("/mapTestT")
    @ApiOperation(value = "传入对象，以map获取值", notes = "传入对象，以map获取值")
	public  Result<Map> mapTestT(@RequestBody Student t){
		jedisClient.set(UserKey.getById, "map",t);
		log.info("键值是否存在：{}",jedisClient.exists(UserKey.getById, "map"));
		redisService.getResult();
		return Result.success(jedisClient.get(UserKey.getById, "map",Map.class));
	}
}
