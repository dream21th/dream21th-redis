package com.dream21th.dream21thredis.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.alibaba.fastjson.JSON;
import com.dream21th.dream21thredis.dto.Student;
import com.dream21th.dream21thredis.result.Result;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/redisTemplate")
@Api(tags="redisTemplate操作")
public class RedisTemplateController {

    @Autowired
	private RedisTemplate<Object, Object> redisTemplate;
    
    @PostMapping("/{key}/{value}")
	@ResponseBody
	public Result redisTemplate(@PathVariable("key") String key,@PathVariable("value") String value){
		log.info("键：【{}】，值：【{}】",key,value);
    	redisTemplate.opsForValue().set(key, value);
		return Result.success(redisTemplate.opsForValue().get(key));
	}
   
    @PostMapping("/{key}")
	@ResponseBody
	public Result<Student> redisTemplateObject(@PathVariable("key") String key,@RequestBody Student student){
		log.info("键：【{}】，值：【{}】",key,JSON.toJSONString(student));
    	redisTemplate.opsForValue().set(key, student);
		return Result.success((Student)redisTemplate.opsForValue().get(key));
	}
    
    @PostMapping("/list/{keyList}")
	@ResponseBody
	public Result<List<Student>> redisTemplateList(@PathVariable("keyList") String keyList,@RequestBody List<Student> student){
		log.info("键：【{}】，值：【{}】",keyList,JSON.toJSONString(student));
    	redisTemplate.opsForValue().set(keyList, student);
		return Result.success((List<Student>)redisTemplate.opsForValue().get(keyList));
	}
    
    @PostMapping("/decrInc/{key}/{num}")
	@ResponseBody
	public Result<Integer> redisTemplatedecrInc(@PathVariable("key") String key,@PathVariable("num") String num){
		log.info("键：【{}】",key);
		redisTemplate.opsForValue().set(key, 1000);
    	log.info("加1后的值：【{}】",redisTemplate.opsForValue().increment(key, 1));
    	log.info("减10后的值：【{}】",redisTemplate.opsForValue().increment(key, -10));
		return Result.success((Integer)redisTemplate.opsForValue().get(key));
	}
    
    @PostMapping("/map/{key1}/{key2}")
	@ResponseBody
	public Result redisTemplateMap(@PathVariable("key1") String key1,@PathVariable("key2") String key2,@RequestBody Map map){
		log.info("键1：【{}】,键2：【{}】，值：【{}】",key1,key2,JSON.toJSONString(map));
    	redisTemplate.opsForHash().put(key1, key2, map);
		return Result.success(redisTemplate.opsForHash().get(key1, key2));
	}
    
    @PostMapping("/mapdelete/{key1}/{key2}")
	@ResponseBody
	public Result redisTemplateMapdelete(@PathVariable("key1") String key1,@PathVariable("key2") String key2){
		log.info("键1：【{}】,键2：【{}】",key1,key2);
		return Result.success(redisTemplate.opsForHash().delete(key1, key2));
	}
    
    @PostMapping("/list/{key1}/{key2}")
	@ResponseBody
	public Result redisTemplateList(@PathVariable("key1") String key1,@PathVariable("key2") String key2){
		log.info("键1：【{}】,键2：【{}】",key1,key2);
		redisTemplate.opsForList().leftPush(key1, key2);
		return Result.success(redisTemplate.opsForList().range(key1, 0, -1));
	}
    
}
