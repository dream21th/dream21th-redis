package com.dream21th.dream21thredis.controller;

import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.dream21th.dream21thredis.dto.MutiDto;
import com.dream21th.dream21thredis.service.RedisLockService;

@RestController
@RequestMapping("/redisLock")
public class RedisLockController {

	@Autowired
	private RedisLockService redisLockService;
	
	@PostMapping("/testLock")
	public void testRedisLock(){
		redisLockService.run();
	}
	
	@GetMapping("/testLock1")
	public void testRedisLock1(String a){
		
		IntStream.range(0, 100).forEach(i->{
			Thread run=new Thread(new Runnable() {
				@Override
				public void run() {
					redisLockService.desc("100");
				}
			});
			
			run.start();
		});
	}
	
	@PostMapping("/testMuti")
	public void testMuti(@RequestBody MutiDto mutiDto){
		
	}
	
	@PostMapping("/error")
	public String error(){
		return "error";
	}
}
