package com.dream21th.dream21thredis.controller;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.dream21th.dream21thredis.dto.Person;
import com.dream21th.dream21thredis.dto.Student;
import com.dream21th.dream21thredis.key.UserKey;
import com.dream21th.dream21thredis.redis.IJedisClient;
import com.dream21th.dream21thredis.result.Result;
import com.dream21th.dream21thredis.service.RedisService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/mutilNode")
@Api(tags="多节点redis操作")
public class RedisMutilNodeController {

	@Autowired
	private IJedisClient jedisClusterClient;
	@Autowired
	private RedisService redisService;
	
    @PostMapping("/mapTest/{key}")
    @ApiOperation(value = "map测试", notes = "map测试")
	public Result<Map<String,String>> mapTest(@PathVariable("key") String key,@RequestBody Map<String,String> map){
		jedisClusterClient.setMap(key, map,0);
		return Result.success(jedisClusterClient.getMap(key));
	}
	
    @PostMapping("/listTest/{key}")
	public Result<List<String>> ListTest(@PathVariable("key") String key,@RequestBody List<String> list){
		jedisClusterClient.setList(key, list, 0);
		return Result.success(jedisClusterClient.getList(key));
	}
    
    @PostMapping("/listTestObj/{key}")
	public Result<List<Object>> listTestObj(@PathVariable("key") String key,@RequestBody List<Object> list){
		jedisClusterClient.setObjectList(key, list, 0);
		return Result.success(jedisClusterClient.getObjectList(key));
	}
    
    @PostMapping("/listTestT")
	public  Result<Student> listTestT(@RequestBody Student t){
		jedisClusterClient.set(UserKey.getById, "name",t);
		log.info("键值是否存在：{}",jedisClusterClient.exists(UserKey.getById, "name"));
		redisService.getResult();
		return Result.success(jedisClusterClient.get(UserKey.getById, "name",Student.class));
	}
    
    @PostMapping("/decIcrTest")
	public  Result<Long> decIcrTest(long num){
		jedisClusterClient.set(UserKey.getById, "count",num);
		log.info("键值是否存在：{}",jedisClusterClient.exists(UserKey.getById, "count"));
		log.info("加1后值：{}",jedisClusterClient.incr(UserKey.getById, "count"));
		log.info("减1后值：{}",jedisClusterClient.decr(UserKey.getById, "count"));
		return Result.success(jedisClusterClient.get(UserKey.getById, "count",Long.class));
	}
    
    @PostMapping("/testObj")
	public  Result<Student> testObj(@RequestBody Student t){
    	jedisClusterClient.setObject("student", t, 0);
		return Result.success((Student)jedisClusterClient.getObject("student"));
	}
    
    @PostMapping("/testT")
	public  Result<Student> testT(@RequestBody Student t){
    	jedisClusterClient.setObject("student", t, 0);
		return Result.success(jedisClusterClient.getT("student"));
	}
    
    @PostMapping("/testT1")
	public  Result<Person> testT1(@RequestBody Person t){
    	jedisClusterClient.setT("person", t, 0);
		return Result.success(jedisClusterClient.getT("person"));
	}
    
    @PostMapping("/testList")
	public  Result<List<Student>> testList(@RequestBody Student t){
		return Result.success(redisService.getList());
	}
    
    @PostMapping("/mapTestT")
    @ApiOperation(value = "传入对象，以map获取值", notes = "传入对象，以map获取值")
	public  Result<Map> mapTestT(@RequestBody Student t){
		jedisClusterClient.set(UserKey.getById, "map",t);
		log.info("键值是否存在：{}",jedisClusterClient.exists(UserKey.getById, "map"));
		redisService.getResult();
		return Result.success(jedisClusterClient.get(UserKey.getById, "map",Map.class));
	}
}
