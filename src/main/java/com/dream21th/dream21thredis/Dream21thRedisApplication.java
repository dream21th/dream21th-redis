package com.dream21th.dream21thredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Dream21thRedisApplication {

	public static void main(String[] args) {
		SpringApplication.run(Dream21thRedisApplication.class, args);
	}
}
